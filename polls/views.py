from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from django.http import Http404
from django.shortcuts import get_object_or_404, render

from .models import Choice, Question

from django.urls import reverse

#При добавлении классов часть 4
from django.views import generic
""" Начало. Эти view принимают аргумент( в отличии от def index)
     создаются новые  разделы сайта под polls/34 такие как:
      /polls/34/results/  и  /polls/34/vote/  На которых текст выводят
       данные функции
#КАК РАБОТАЕТ?
 Когда кто-то запрошивает страницу с сайта скажем /polls/34/
  Django загрузит модуль python my_own_site.urls потому что на него
   указывает параметр urls в основном файле
    Он находит в  polls переменную с именем urlpatterns и просматривает
     шаблоны по порядку.

  Найдя совпадение в 'polls/' он удаляет соответствующий текст ('polls/')
   И отправляет оставшийся текст - '34/'  в URLconf для 'polls.urls'
    Для дальнейшей обработки. Там это соответствует  '<int:question_id>/'
     В результате чего вызывается detail()
      Но и это еще не все!"""

"""Часть question_id = 34 взята из <int: question_id>.
     Использование угловых скобок «захватывает» часть URL и
      отправляет ее в качестве аргумента ключевого слова в функцию
        представления. Часть строки: question_id> определяет имя,
         которое будет использоваться для идентификации сопоставленного
          шаблона, а часть <int: - это преобразователь, который определяет,
           какие шаблоны должны соответствовать этой части пути URL."""


"""Raising a 404 error"""

"""Теперь давайте рассмотрим подробный вид вопроса - страницу,
     которая отображает текст вопроса для данного опроса.
      Вот мнение: опросы / views.py"""

"""Новая концепция здесь: представление вызывает исключение Http404,
          если вопрос с запрошенным идентификатором не существует."""

"""Это очень распространенная идиома - использовать get () и вызывать Http404,
    если объект не существует. Джанго предоставляет ярлык.
     Вот представление detail (), переписанное:"""

"""Функция get_object_or_404 () принимает модель Django в качестве первого
    аргумента и произвольное количество аргументов ключевого слова, которое она
     передает функции get () менеджера модели. Он вызывает Http404,
      если объект не существует."""


#часть 4
"""     def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})    """

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        ####Return the last five published questions.
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'
#часть 4
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
#Окончаение"""

"""Этот код (def vote) включает в себя несколько вещей,
    которые мы еще не рассмотрели в этом руководстве:"""

"""request.POST - это словарь-объект, который позволяет вам получить доступ
    к отправленным данным по имени ключа.
     В этом случае request.POST ['choice'] возвращает идентификатор
      выбранного варианта в виде строки. Значения request.POST всегда являются строками."""
"""Обратите внимание, что Django также предоставляет request.GET
    для доступа к данным GET таким же образом, но мы явно используем request.POST
     в нашем коде, чтобы гарантировать, что данные изменяются только через вызов POST."""
"""request.POST ['choice'] вызовет KeyError, если выбор не был предоставлен
    в данных POST. Приведенный выше код проверяет наличие KeyError и
     повторно отображает форму вопроса с сообщением об ошибке,
      если выбор не предоставлен."""
"""После увеличения счетчика выбора код возвращает HttpResponseRedirect,
    а не обычный HttpResponse. HttpResponseRedirect принимает один аргумент: URL-адрес,
     на который будет перенаправлен пользователь
      (см. Следующий пункт, как мы создаем URL-адрес в этом случае)."""
"""Как указано выше в комментарии Python, вы должны всегда возвращать HttpResponseRedirect
    после успешной обработки данных POST. Этот совет не является
     специфическим для Django; это просто хорошая практика веб-разработки."""
"""В этом примере мы используем функцию reverse () в конструкторе HttpResponseRedirect.
    Эта функция помогает избежать жесткого кодирования URL-адреса в функции просмотра.
     Ему дается имя представления, которому мы хотим передать управление,
      и переменная часть шаблона URL, которая указывает на это представление.
       В этом случае, используя URLconf, который мы настроили в Уроке 3,
        этот вызов reverse () вернет строку вроде  '/polls/3/results/'
        где 3 - это значение question.id. Этот перенаправленный URL-адрес
         затем вызовет представление «Результаты» для отображения последней страницы."""

"""Обратите внимание, что после того, как мы сделали это во всех
    этих представлениях, нам больше не нужно импортировать загрузчик и HttpResponse
     (вы захотите сохранить HttpResponse, если у вас все еще есть методы заглушки
      для детализации, результатов и голосования)."""



"""Функция render () принимает объект запроса в качестве первого аргумента,
    имя шаблона в качестве второго аргумента и
     словарь в качестве необязательного третьего аргумента.
      Он возвращает объект HttpResponse данного шаблона,
       отображенный с заданным контекстом."""
"""днако здесь есть проблема: дизайн страницы жестко закодирован в представлении. Если вы хотите изменить внешний вид страницы, вам придется редактировать этот код Python. Итак, давайте используем систему шаблонов Django, чтобы отделить дизайн от Python, создав шаблон, который может использовать представление.

Сначала создайте каталог с именем templatesв вашем pollsкаталоге. Джанго будет искать там шаблоны.

TEMPLATESНастройки вашего проекта описывают, как Django будет загружать и отображать шаблоны. Файл настроек по умолчанию настраивает DjangoTemplates серверную часть, для которой APP_DIRSзадан параметр True. По соглашению DjangoTemplatesищет подкаталог «templates» в каждом из INSTALLED_APPS.

В templatesкаталоге, который вы только что создали, создайте другой каталог с именем pollsи в нем создайте файл с именем index.html. Другими словами, ваш шаблон должен быть на polls/templates/polls/index.html. Из-за того, как app_directories загрузчик шаблонов работает, как описано выше, вы можете ссылаться на этот шаблон в Django просто как polls/index.html."""
#часть 4
"""def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)"""

"""После того, как кто-то проголосовал в вопросе,
    представление voice () перенаправляет на страницу результатов для вопроса.
     Давайте напишем эту точку зрения:"""

"""Это почти точно так же, как представление detail () из Tutorial 3.
    Единственное отличие - это имя шаблона. Мы исправим эту избыточность позже."""
