
from django.urls import path

from . import views

"""У учебного проекта есть только одно приложение, опросы.
    В реальных проектах Django может быть пять, десять, двадцать приложений
     ли больше. Как Django различает имена URL между ними?
      Например, приложение опросов имеет подробное представление,
       как и приложение того же проекта, что и для блога.
        Как сделать так, чтобы Django знал, какое представление приложения
         cоздавать для URL при использовании тега шаблона {% url%}?"""
"""Ответ заключается в том, чтобы добавить пространства имен в ваш URLconf.
    В файле polls / urls.py добавьте имя_приложения,
     чтобы задать пространство имен приложения:"""
app_name = 'polls'
app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
